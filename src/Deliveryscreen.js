//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, Image, TextInput, Button, Alert, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { changeFood, changeTotalprice, changePiece } from '../redux/actions';
import { Rating, AirbnbRating } from 'react-native-ratings';
import Icon from 'react-native-vector-icons/Ionicons';
// create a component
class Deliveryscreen extends Component {

    constructor(props) {
        super(props);
        this.state = {

            Rating: '',
            Like: '',
            Food: '',
            Inputpiece: 0,
        };
    }

    Kliktotal = () => {
        if (this.props.piece == 0) {
            Alert.alert('Wajib isi')
        }
        else {
            const totalharga = this.props.piece * this.props.price.price
            const stringharga = totalharga.toString()
            this.props.changeTotalprice({ totalprice: stringharga })
            this.props.navigation.navigate('Totalscreen');
        }
    }


    render() {
        return (
            <SafeAreaView>
                <View style={{ backgroundColor: 'white' }}>

                    <Image source={{ uri: this.props.image.image }} style={styles.image} />
                    <View style={{ borderColor: 'gray', borderWidth: 2 }}></View>
                    <Text style={styles.foodName}>{this.props.food.food}</Text>

                    <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                        <Rating
                            type='custom'
                            startingValue={this.props.rating.rating}
                            ratingCount={5}
                            imageSize={20}
                            onFinishRating={this.ratingCompleted}
                            style={{ marginRight: 5 }}
                            ratingColor='red'
                        />

                        <Text style={{ marginLeft: 5, fontSize: 18 }}>{this.props.rating.rating}</Text>
                    </View>

                    <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 5 }}>
                        <Icon style={[{ color: 'red' }]} size={20} name={'ios-person'} />
                        <Text style={{ fontSize: 17, marginLeft: 5 }}>{'People Like : ' + this.props.like.like}</Text>
                    </View>

                    <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 10 }}>
                        <Text style={{ fontSize: 20 }}>Harga : </Text>
                        <Text style={{ fontSize: 20 }}>{this.props.price.price + ' * '}</Text>
                        <TextInput
                            style={styles.textInput}
                            // value={this.state.Inputpiece}
                            onChangeText={this.props.changePiece}
                        />
                    </View>

                    <View style={{ flexDirection: 'row', paddingTop: 10, alignSelf: 'center' }}>
                        <Icon style={[{ color: 'green' }]} size={25} name={'ios-cash'} />
                        <Text style={{ fontSize: 20, marginLeft: 5 }}>Total Harga : </Text>
                        <Text style={{ fontSize: 20 }}>{this.props.piece * this.props.price.price}</Text>
                    </View>

                    <TouchableOpacity onPress={() => this.Kliktotal()}>
                        <View style={styles.total}>
                            <Text style={{ color: 'white', textAlign: 'center' }}>
                                Pembayaran
                            </Text>
                        </View>
                    </TouchableOpacity>

                </View>
            </SafeAreaView >
        );
    }
}



// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
    image: {
        height: '62%',
        width: '100%',
        borderRadius: 5,
    },
    foodName: {
        fontSize: 30,
        marginHorizontal: 10,
        marginVertical: 10,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    textInput: {
        borderColor: '#20B2AA',
        borderWidth: 2,
        paddingVertical: 2,
        paddingHorizontal: 20,
        borderRadius: 10
    },
    total: {
        margin: 20,
        paddingVertical: 10,
        borderColor: '#5F9EA0',
        borderWidth: 1,
        borderRadius: 50,
        backgroundColor: '#5F9EA0',
        paddingHorizontal: 35
    },
});

const mapStateToProps = (state) => ({
   
    food: state.data.food,
    image: state.data.image,
    rating: state.data.rating,
    price: state.data.price,
    like: state.data.like,
    totalprice: state.data.totalprice,
    piece: state.data.piece
})

const mapDispatchToProps = (dispatch) => ({
    changeFood: (value) => dispatch(changeFood(value)),
    changeImage: (value) => dispatch(changeImage(value)),
    changeRating: (value) => dispatch(changeRating(value)),
    changePrice: (value) => dispatch(changePrice(value)),
    changeLike: (value) => dispatch(changeLike(value)),
    changeTotalprice: (value) => dispatch(changeTotalprice(value)),
    changePiece: (value) => dispatch(changePiece(value)),
})


export default connect(mapStateToProps, mapDispatchToProps)(Deliveryscreen)

//make this component available to the app
// export default Deliveryscreen;
