//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Image, ImageBackground, TextInput, Button, SafeAreaView, Linking, Alert,StatusBar } from 'react-native';
import axios from 'axios';
// import StarRating from 'react-native-star-rating';
import { Rating, AirbnbRating } from 'react-native-ratings';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { changeFood, changeImage, changePrice, changeRating, changeLike } from '../redux/actions';

console.disableYellowBox = true;

// create a component
class Searchscreen extends Component {


    constructor(props) {
        super(props);
        this.state = {

            fooddata: [],
            rating: null,
            mieayamlike: '',
            starCount: 3.5,
            searchgoogle: '',
            searchmenu: '',
        };
    }


    componentDidMount() {

        // https://www.google.com/search?q=testdata&cad=h

        axios.get('https://bagusputra.000webhostapp.com/foodapi.json')
            .then((res) => {

                console.log(res.data.Food)

                this.setState({
                    fooddata: res.data.Food,

                })

            })
            .catch((err) => {
                // handle err
                // console.log(err)
            });


    }

    onclick_item(item) {

        this.props.navigation.navigate('Deliveryscreen');
        this.props.changeFood({ food: item.Food })
        this.props.changeRating({ rating: item.Rating })
        this.props.changeLike({ like: item.Like })
        this.props.changePrice({ price: item.Price })
        this.props.changeImage({ image: item.Image })
    }

    renderItem = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => this.onclick_item(item)}>
                <View style={styles.viewRender}>
                    <Image source={{ uri: item.Image }} style={styles.imageflatlist} />
                    <Text style={{ fontWeight: 'bold', marginTop: 3, fontSize: 20 }}>{item.Food}</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Rating
                            type='custom'
                            startingValue={item.Rating}
                            ratingCount={5}
                            imageSize={15}
                            onFinishRating={this.ratingCompleted}
                            style={{ marginRight: 5 }}
                            ratingColor='red'
                        />

                        <Text>({item.Like})</Text>

                    </View>
                </View>
            </TouchableOpacity>
        )
    };

   
    getmenu() {
        if (this.state.searchmenu != 'Mieayam' && this.state.searchmenu != 'Miegoreng' && this.state.searchmenu != 'Buburayam') {
            return Alert.alert('Tidak ada data ' + this.state.searchmenu)

        } else {
            this.props.navigation.navigate(`${this.state.searchmenu}`);

        }
    }


    render() {
        return (

            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <StatusBar barStyle="light-content" />
                <ImageBackground
                    source={require('../image/warteg.jpeg')}
                    style={{ height: 320, width: '100%' }}
                >
                    <View style={styles.backImage}>
                        <Text style={styles.textBackImage}>New and Popular in</Text>
                        <Text style={styles.textBackImage}>Citayam</Text>
                    </View>
                    <View style={styles.textInputBackImage}>
                        <TouchableOpacity onPress={() => { Linking.openURL(`https://www.google.com/search?q=${this.state.searchgoogle}&cad=h`) }}>
                            <Icon style={{ color: 'white', marginLeft: 10 }} size={15} name={'ios-search'} />
                        </TouchableOpacity>
                        <TextInput style={styles.inputTextColorOne}
                            placeholder='Hot & new Restaurants'
                            placeholderTextColor='white'
                            onChangeText={(searchgoogle) => this.setState({ searchgoogle })}
                        />
                    </View>
                </ImageBackground>

                <View style={styles.textInputMiddle}>
                    <TouchableOpacity onPress={() => this.getmenu()}>
                        <Icon style={{ color: 'gray', marginLeft: 10 }} size={15} name={'ios-search'} />
                    </TouchableOpacity>
                    <TextInput style={styles.inputTextColorTwo}
                        placeholder='Find Burger, others on yelp'
                        placeholderTextColor='gray'
                        onChangeText={(searchmenu) => this.setState({ searchmenu })}
                    />
                </View>

                <View style={{ backgroundColor: 'white', paddingTop: 0, marginHorizontal:10 }}>
                    <View style={styles.iconSelect}>
                        <View>
                            <Icon style={{ color: 'red', alignSelf: 'center' }} size={25} name={'ios-cafe'} />
                            <Text style={{ textAlign: 'center' }}>Restaurants</Text>
                        </View>
                        <View>
                            <Icon style={{ color: 'green', alignSelf: 'center' }} size={25} name={'ios-moon'} />
                            <Text style={{ textAlign: 'center' }} >Date Night</Text>
                        </View>
                        <View>
                            <Icon style={{ color: 'blue', alignSelf: 'center' }} size={25} name={'ios-hand'} />
                            <Text style={{ textAlign: 'center' }}>Massage</Text>
                        </View>
                        <View>
                            <Icon style={{ color: 'purple', alignSelf: 'center' }} size={25} name={'ios-key'} />
                            <Text style={{ textAlign: 'center' }}>Locksmiths</Text>
                        </View>
                    </View>
                    <View style={styles.iconSelect}>

                        <View style={{ paddingHorizontal: 11 }}>
                            <Icon style={{ color: 'pink', alignSelf: 'center' }} size={25} name={'ios-bicycle'} />
                            <Text style={{ textAlign: 'center' }}>Delivery</Text>
                        </View>
                        <View style={{ marginLeft: 10 }}>
                            <Icon style={{ color: 'purple', alignSelf: 'center' }} size={25} name={'ios-cart'} />
                            <Text style={{ textAlign: 'center' }} >Takeout</Text>
                        </View>
                        <View style={{ marginRight: 8 }}>
                            <Icon style={{ color: 'orange', alignSelf: 'center' }} size={25} name={'ios-calendar'} />
                            <Text style={{ textAlign: 'center' }}>Reservations</Text>
                        </View>
                        <View style={{ marginRight: 20 }}>
                            <Icon style={{ color: 'green', alignSelf: 'center' }} size={25} name={'ios-menu'} />
                            <Text style={{ textAlign: 'center' }}>More</Text>
                        </View>
                    </View>
                </View>

                <View style={{ backgroundColor: '#FCFCFC' }}>
                    <View style={styles.findTitle}>
                        <Text style={{ fontWeight: 'bold', fontSize: 20, marginLeft:10 }}>Find a Table on Yelp</Text>
                        <Icon style={{ color: 'black', alignSelf: 'center',marginRight:5 }} size={25} name={'ios-menu'} />
                    </View>

                    <View style={{ flexDirection: 'row', marginHorizontal: 20 }}>
                        <Text >Reserve now, skip the line later. </Text>
                        <Icon style={{ color: 'black', alignSelf: 'center', marginTop: -4 }} size={25} name={'ios-information'} />
                    </View>
                </View>

                <FlatList
                    showsHorizontalScrollIndicator={false}
                    horizontal
                    data={this.state.fooddata}
                    renderItem={this.renderItem}
                    style={{ backgroundColor: '#FCFCFC', paddingTop: 10, marginLeft:10 }}
                />

            </View>

        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
    imageflatlist: {
        height: '65%',
        width: '100%',
        borderRadius: 5,
    },
    inputTextColorOne: {
        marginLeft: 10,
        fontSize: 13,
        color: 'white'
    },

    inputTextColorTwo: {
        color: "gray",
        fontSize: 13,
        marginLeft: 19,

    },
    textBackImage: {
        color: 'white',
        fontSize: 25,
        fontWeight: 'bold'
    },
    backImage: {
        marginTop: 50,
        marginLeft: 10,
        marginBottom: 10
    },
    textInputBackImage: {
        flexDirection: 'row',
        borderColor: 'white',
        borderWidth: 2,
        marginLeft: 10,
        borderRadius: 5,
        marginRight: 170,
        padding: 5
    },
    textInputMiddle: {
        flexDirection: 'row',
        borderTopColor: '#F5F5F5',
        borderTopWidth: 1,
        marginHorizontal: 10,
        borderRadius: 5,
        padding: 10,
        marginTop: -20,
        backgroundColor: 'white',
        borderLeftColor: '#F5F5F5',
        borderRightColor: '#F5F5F5',
        borderBottomColor: '#e3e3e3',
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 3,
    },
    iconSelect: {
        flexDirection: 'row',
        marginHorizontal: 10,
        justifyContent: 'space-between',
        paddingVertical: 20
    },
    findTitle: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: 10,
        marginHorizontal: 10
    },
    viewRender: {
        marginHorizontal: 5,
        width: 160,
        backgroundColor: 'white',
        height: 150,
        borderRadius: 10
    },

});

const mapStateToProps = (state) => ({
    food: state.data.food,
    image: state.data.image,
    rating: state.data.rating,
    price: state.data.price,
    like: state.data.like,
})

const mapDispatchToProps = (dispatch) => ({
    changeFood: (value) => dispatch(changeFood(value)),
    changeImage: (value) => dispatch(changeImage(value)),
    changeRating: (value) => dispatch(changeRating(value)),
    changePrice: (value) => dispatch(changePrice(value)),
    changeLike: (value) => dispatch(changeLike(value))
})

export default connect(mapStateToProps, mapDispatchToProps)(Searchscreen)


// export default Searchscreen;