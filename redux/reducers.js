import { combineReducers } from 'redux'
import {
  CHANGE_FOOD,
  CHANGE_IMAGE,
  CHANGE_RATING,
  CHANGE_LIKE,
  CHANGE_PRICE,
  CHANGE_TOTALPRICE,
  CHANGE_PIECE,
} from './actions'

const initialState = {
  food: '',
  image: '',
  price: '',
  rating: '',
  like: '',
  totalprice: '',
  piece: '',
}

function data(state = initialState, action) {
 
  switch (action.type) {

    case CHANGE_FOOD:

      return {
        ...state,
        food: action.food
      }

    case CHANGE_PRICE:

      return {
        ...state,
        price: action.price
      }

    case CHANGE_LIKE:

      return {
        ...state,
        like: action.like
      }

    case CHANGE_RATING:

      return {
        ...state,
        rating: action.rating
      }

    case CHANGE_IMAGE:

      return {
        ...state,
        image: action.image
      }
    case CHANGE_TOTALPRICE:

      return {
        ...state,
        totalprice: action.totalprice
      }

    case CHANGE_PIECE:
      return {
        ...state,
        piece: action.piece
      }


    default:
      return state
  }
}
const foodApp = combineReducers({
  // visibilityFilter,
  data
})
export default foodApp