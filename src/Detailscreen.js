//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { changeUsername, changeJob, changeUmur, changeAlamat,changeFood } from '../redux/actions';

// create a component
class Detailscreen extends Component {
    render() {
        return (
            <View >
                <Text>Detailscreen</Text>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});



  
//make this component available to the app
export default Detailscreen;
