/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { View, Text } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Homescreen from './src/Homescreen';
import Searchscreen from './src/Searchscreen';
import Deliveryscreen from './src/Deliveryscreen';
import Menuscreen from './src/Menuscreen';
import Personscreen from './src/Personscreen';
import Collectionscreen from './src/Collectionscreen';
import Totalscreen from './src/Totalscreen';
import Mieayam from './src/Mieayam';
import Miegoreng from './src/Miegoreng';
import Buburayam from './src/Buburayam';
import Testfirebase from './src/Testfirebase';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';  
import { Provider } from 'react-redux';
import store from './redux/store';


const TabNavigator = createMaterialBottomTabNavigator(
  {
    Searchscreen: {
      screen: Searchscreen,
      navigationOptions: {
        tabBarLabel: 'Search',
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon style={[{ color: tintColor }]} size={25} name={'ios-search'} />
          </View>),
      }
    },
    Deliveryscreen: {
      screen: Deliveryscreen,
      navigationOptions: {
        tabBarLabel: 'Delivery',
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon style={[{ color: tintColor }]} size={25} name={'ios-beer'} />
          </View>),
      }
    },

    Personscreen: {
      screen: Personscreen,
      navigationOptions: {
        tabBarLabel: 'Me',
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon style={[{ color: tintColor }]} size={25} name={'ios-person'} />
          </View>),
      }
    },
    
   Collectionscreen: {
      screen: Collectionscreen,
      navigationOptions: {
        tabBarLabel: 'Collection',
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon style={[{ color: tintColor }]} size={25} name={'ios-bookmark'} />
          </View>),
      }
    },
    Menuscreen: {
      screen: Menuscreen,
      navigationOptions: {
        tabBarLabel: 'Menu',
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon style={[{ color: tintColor }]} size={25} name={'ios-list'} />
          </View>),
      }
    },
    
  },
  {
    initialRouteName: "Searchscreen",
    activeColor: 'red',
    inactiveColor: 'black',
    barStyle: { backgroundColor: 'white' },
  },
);

const AppNavigator = createStackNavigator({
  Searchscreen: {
    screen: TabNavigator,
    navigationOptions: { headerShown: false }
  },
  Homescreen: {
    screen: Homescreen,
    navigationOptions: { headerShown: false }
  },
  Totalscreen: {
    screen: Totalscreen,
    navigationOptions: { headerShown: false }
  },
  Mieayam: {
    screen: Mieayam,
    
  },
  Miegoreng: {
    screen: Miegoreng,
    
  },
  Buburayam: {
    screen: Buburayam,
    
  },
  Testfirebase:{
    screen:Testfirebase,
  },
});

// export default createAppContainer(AppNavigator);

const Root = createAppContainer(AppNavigator);

const App = () => (
  <Provider store={store}>
    <Root />
  </Provider>
)

export default App;