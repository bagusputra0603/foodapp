//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, SafeAreaView } from 'react-native';
import firebase from 'firebase';
import { connect } from 'react-redux';

// create a component
class Testfirebase extends Component {

    constructor(props) {
        super(props);
        this.state = {


            // Food: '',
            flatlistview: '',
        };
    }

    componentWillMount() {
        var firebaseConfig = {
            apiKey: "AIzaSyAxMIprz1E75g_Xw4drAij5vE092D1z8e8",
            authDomain: "foodapp-51f57.firebaseapp.com",
            databaseURL: "https://foodapp-51f57.firebaseio.com",
            projectId: "foodapp-51f57",
            storageBucket: "foodapp-51f57.appspot.com",
            messagingSenderId: "428234301198",
            appId: "1:428234301198:web:4177cdc3140be5ef05015f",
            measurementId: "G-DX1YNT0689"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);

        console.log(firebase)
        firebase.database().ref('users').set({
            id:1,
            food: this.props.food.food,
            rating: this.props.rating.rating,
        }).then(() => {
            console.log('Inserted');
        }).catch((error) => {
            console.log(error)
        });

        firebase.database().ref('users').on('value', (data) => {
            console.log(data.toJSON());

            this.setState({
                
            })


        })

   

    }


    render() {
        return (
            <SafeAreaView>
                <View>
                    <Text>Test data</Text>
{/* 
                    <FlatList
                   
                    data={this.state.flatlistview}
                    renderItem={({item}) => <Text>{item.rating}</Text>} */}
                    
                />
                </View>
            </SafeAreaView>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

const mapStateToProps = (state) => ({
    username: state.data.username,
    umur: state.data.umur,
    job: state.data.job,
    alamat: state.data.alamat,
    food: state.data.food,
    image: state.data.image,
    rating: state.data.rating,
    price: state.data.price,
    like: state.data.like,
    totalprice: state.data.totalprice,
    piece: state.data.piece
})

export default connect(mapStateToProps)(Testfirebase)
//make this component available to the app
// export default Testfirebase;
