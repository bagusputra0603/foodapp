//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet,Image,TouchableOpacity } from 'react-native';

// create a component
class Mieayam extends Component {
    render() {
        return (
            <View style={styles.background}>
            <Image source={{ uri: 'https://selerasa.com/wp-content/uploads/2015/05/images_mie_Mie_ayam_14-mie-ayam-kampung-1200x720.jpg' }} style={styles.image} />
            <View style={{ borderColor: 'gray', borderWidth: 2 }}></View>
            <Text style={styles.title}>Mie Ayam</Text>
            <Text style={{ marginHorizontal: 10, fontSize: 15, }}>Mi ayam atau bakmi ayam adalah masakan Indonesia yang terbuat dari mi kuning direbus mendidih kemudian ditaburi saus kecap khusus beserta daging ayam dan sayuran. Mi Ayam terkadang ditambahi dengan bakso, pangsit dan jamur. Mi berasal dari Tiongkok tetapi mi ayam yang serupa di Indonesia tidak ditemukan di Tiongkok. Mi ayam aslinya dari Tiongkok Selatan terutama dari daerah-daerah pelabuhan seperti Fujian dan Guandong.Meskipun mi bukan asli Indonesia tapi nyatanya kini mi ayam seakan sudah menjadi makanan tradisional Indonesia. Makanan ini sudah tersebar di seluruh Indonesia, terutama di daerah Jawa makanan ini sangat mudah di temukan. Penjual mi ayam di Indonesia yang populer berasal dari Wonogiri.</Text>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Searchscreen')}>
                <View style={styles.back}>
                    <Text style={{ color: 'white', textAlign: 'center' }}>
                       Back To Menu
                        </Text>
                </View>
            </TouchableOpacity>
        </View>

        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        height: '50%',
    },
    background: {
        height: '100%',
        backgroundColor:'white'
    },
    image: {
        height: '30%'
    },
    title: {
        textAlign: 'center', fontSize: 30, fontWeight: 'bold', marginBottom: 10, marginTop: 10
    },
    back: {
        margin: 20,
        paddingVertical: 10,
        borderColor: '#5F9EA0',
        borderWidth: 1,
        borderRadius: 50,
        backgroundColor: '#5F9EA0',
        paddingHorizontal: 35
    },
});

//make this component available to the app
export default Mieayam;
