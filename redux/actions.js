export const CHANGE_FOOD ='CHANGE_FOOD'
export const CHANGE_LIKE ='CHANGE_LIKE'
export const CHANGE_RATING ='CHANGE_RATING'
export const CHANGE_IMAGE ='CHANGE_IMAGE'
export const CHANGE_PRICE ='CHANGE_PRICE'
export const CHANGE_TOTALPRICE ='CHANGE_TOTALPRICE'
export const CHANGE_PIECE ='CHANGE_PIECE'

export function changeFood(food) {

  return { type:CHANGE_FOOD , food }
}

export function changeLike(like) {
  
  return { type:CHANGE_LIKE , like }
}

export function changeRating(rating) {
 
  return { type:CHANGE_RATING , rating }
}

export function changePrice(price) {
 
  return { type:CHANGE_PRICE , price }
}

export function changeImage(image) {
  
  return { type:CHANGE_IMAGE , image }
}

export function changeTotalprice(totalprice) {
 
  return { type:CHANGE_TOTALPRICE , totalprice }
}

export function changePiece(piece) {
 
  return { type:CHANGE_PIECE , piece }
}