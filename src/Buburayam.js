//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image,TouchableOpacity } from 'react-native';


// create a component
class Buburayam extends Component {
    render() {
        return (

            <View style={styles.background}>
                <Image source={{ uri: 'https://i2.wp.com/resepkoki.id/wp-content/uploads/2017/09/Resep-Bubur-Ayam.jpg?fit=1300%2C1300&ssl=1' }} style={styles.image} />
                <View style={{ borderColor: 'gray', borderWidth: 2 }}></View>
                <Text style={styles.title}>Bubur Ayam</Text>
                <Text style={{ marginHorizontal: 10, fontSize: 15, }}>Bubur ayam adalah salah satu jenis makanan bubur dari Indonesia. Bubur nasi adalah beras yang dimasak dengan air yang banyak sehingga memiliki tekstur yang lembut dan berair. Bubur biasanya disajikan dalam suhu panas atau hangat. Bubur ayam disajikan dengan irisan daging ayam dengan beberapa bumbu, seperti kecap asin dan kecap manis, merica, garam, dan kadang-kadang diberi kaldu ayam. Bubur dilengkapi dengan taburan daun bawang cincang, bawang goreng, seledri, tongcai (sayur asin), kedelai goreng, cakwe, dan kerupuk. Bubur ayam cocok bagi mereka yang kurang menyukai masakan Indonesia yang pedas, karena bubur umumnya tidak pedas; sambal atau saus cabe disajikan secara terpisah.</Text>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Searchscreen')}>
                    <View style={styles.back}>
                        <Text style={{ color: 'white', textAlign: 'center' }}>
                           Back To Menu
                            </Text>
                    </View>
                </TouchableOpacity>
            </View>

        );
    }
}

// define your styles
const styles = StyleSheet.create({
    background: {
        height: '100%',
        backgroundColor:'white'
    },
    image: {
        height: '30%'
    },
    title: {
        textAlign: 'center', fontSize: 30, fontWeight: 'bold', marginBottom: 10, marginTop: 10
    },
    back: {
        margin: 20,
        paddingVertical: 10,
        borderColor: '#5F9EA0',
        borderWidth: 1,
        borderRadius: 50,
        backgroundColor: '#5F9EA0',
        paddingHorizontal: 35
    },
});

//make this component available to the app
export default Buburayam;
