//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';

// create a component
class Miegoreng extends Component {
    render() {
        return (
            // <View style={styles.container}>
            //      <Image source={{ uri: 'https://selerasa.com/wp-content/uploads/2015/09/images_mancanegara_Bakmi_bakmi-goreng3-1200x720.jpg' }} style={styles.container} />
            //       <Text>Mie Goreng</Text>
            //       <Text>Mi goreng berarti "mi yang digoreng" [1] adalah makanan yang populer dan digemari di Indonesia, Malaysia, dan Singapura. Mi goreng terbuat dari mi kuning yang digoreng dengan sedikit minyak goreng, dan ditambahkan bawang putih, bawang merah, udang serta daging ayam atau daging sapi, irisan bakso, cabai, sayuran, tomat, telur ayam, dan acar. Makanan ini sangat populer dan dapat ditemui di mana-mana di Indonesia, mulai dari pedagang pinggir jalan (kaki lima) sampai restoran mewah. Mi goreng juga dapat ditemukan di warung mamak di Malaysia dan Singapura. Versi mi instan dari mi goreng juga sangat populer, seperti Indomie Mi goreng.</Text>
            // </View>

            <View style={styles.background}>
                <Image source={{ uri: 'https://selerasa.com/wp-content/uploads/2015/09/images_mancanegara_Bakmi_bakmi-goreng3-1200x720.jpg' }} style={styles.image} />
                <View style={{ borderColor: 'gray', borderWidth: 2 }}></View>
                <Text style={styles.title}>Mie Goreng</Text>
                <Text style={{ marginHorizontal: 10, fontSize: 15, }}>Mi goreng berarti "mi yang digoreng" [1] adalah makanan yang populer dan digemari di Indonesia, Malaysia, dan Singapura. Mi goreng terbuat dari mi kuning yang digoreng dengan sedikit minyak goreng, dan ditambahkan bawang putih, bawang merah, udang serta daging ayam atau daging sapi, irisan bakso, cabai, sayuran, tomat, telur ayam, dan acar. Makanan ini sangat populer dan dapat ditemui di mana-mana di Indonesia, mulai dari pedagang pinggir jalan (kaki lima) sampai restoran mewah. Mi goreng juga dapat ditemukan di warung mamak di Malaysia dan Singapura. Versi mi instan dari mi goreng juga sangat populer, seperti Indomie Mi goreng.</Text>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Searchscreen')}>
                    <View style={styles.back}>
                        <Text style={{ color: 'white', textAlign: 'center' }}>
                            Back To Menu
            </Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        height: '50%'
    },
    background: {
        height: '100%',
        backgroundColor:'white'

    },
    image: {
        height: '30%'
    },
    title: {
        textAlign: 'center', fontSize: 30, fontWeight: 'bold', marginBottom: 10, marginTop: 10
    },
    back: {
        margin: 20,
        paddingVertical: 10,
        borderColor: '#5F9EA0',
        borderWidth: 1,
        borderRadius: 50,
        backgroundColor: '#5F9EA0',
        paddingHorizontal: 35
    },
});

//make this component available to the app
export default Miegoreng;
