import { createStore } from 'redux'
import foodApp from './reducers'
const store = createStore(foodApp)

export default store;